#include "LittleFS.h" // LittleFS is declared
#include <ArduinoJson.h> // https://github.com/bblanchon/ArduinoJson
#include <Bounce2.h>
#include <ESP8266WiFi.h> //https://github.com/esp8266/Arduino

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WebSocketsClient.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager

#define TRIGGER D0
#define BUTTON D1
#define USE_SERIAL Serial

bool shouldSaveConfig = false;
long previousMillis = 0;
long interval = 1000 * 60 * 5;
WebSocketsClient webSocket;
Bounce b = Bounce();

void triggerMp3() {
  Serial.println("Activating mp3 trigger");
  digitalWrite(TRIGGER, LOW);
  delay(100);
  digitalWrite(TRIGGER, HIGH);
}

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length) {
  switch (type) {
  case WStype_DISCONNECTED:
    USE_SERIAL.printf("[WSc] Disconnected!\n");
    break;
  case WStype_CONNECTED: {
    USE_SERIAL.printf("[WSc] Connected to url: %s\n", payload);

    StaticJsonDocument<128> doc;
    doc["action"] = "subscribe";
    doc["key"] = "e985bc230dde4517cf9f2d3650dd516c247617be357999604a077bded4484b1f"; // path: 'meeseeksbox/playRandom'
    String json;
    serializeJson(doc, json);
    webSocket.sendTXT(json);
  } break;
  case WStype_TEXT:
    if (payload[0] == '{' && payload[length - 1] == '}' && length < 128) {
      StaticJsonDocument<128> doc;
      DeserializationError error = deserializeJson(doc, payload, length);
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.f_str());
        return;
      }
      USE_SERIAL.printf("[WSc] get json:\n");
      serializeJsonPretty(doc, Serial);
      Serial.println("");
      if (doc["value"]) {
        triggerMp3();
      } else if (doc["value"] == false) {
        StaticJsonDocument<128> resp;
        resp["status"] = "online";
        String json;
        serializeJson(resp, json);
        webSocket.sendTXT(json);
      }
    } else {
      USE_SERIAL.printf("[WSc] get text: %s\n", payload);
    }
    break;
  case WStype_BIN:
    USE_SERIAL.printf("[WSc] get binary length: %u\n", length);
    hexdump(payload, length);
    break;
  case WStype_ERROR:
  case WStype_FRAGMENT_TEXT_START:
  case WStype_FRAGMENT_BIN_START:
  case WStype_FRAGMENT:
  case WStype_FRAGMENT_FIN:
  case WStype_PING:
  case WStype_PONG:
    USE_SERIAL.printf("[WSc] other event\n");
    break;
  }
}

void saveConfigCallback() {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void setupLittleFS() {
  //read configuration from FS json
  Serial.println("mounting FS...");

  if (LittleFS.begin()) {
    Serial.println("mounted file system");
    if (LittleFS.exists("/config.json")) {
      //file exists, reading and loading
      Serial.println("reading config file");
      File configFile = LittleFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonDocument jsonBuffer(1024);
        deserializeJson(jsonBuffer, buf.get());
        serializeJson(jsonBuffer, Serial);
        if (jsonBuffer.isNull()) {
          Serial.println("failed to load json config");
        } else {
          Serial.println("\nparsed json");
        }
      }
    }
  } else {
    Serial.println("failed to mount FS");
  }
  //end read
}

void setup() {
  Serial.begin(115200);
  pinMode(TRIGGER, OUTPUT);
  digitalWrite(TRIGGER, HIGH);
  pinMode(BUTTON, INPUT_PULLUP);
  b.attach(BUTTON, INPUT_PULLUP);

  setupLittleFS();

  WiFiManager wifiManager;
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.autoConnect("AutoConnectAP");
  Serial.println("connected...yeey :)");

  if (shouldSaveConfig) {
    Serial.println("saving config");
    DynamicJsonDocument jsonBuffer(1024);

    File configFile = LittleFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    serializeJsonPretty(jsonBuffer, Serial);
    serializeJson(jsonBuffer, configFile);
    configFile.close();
    //end save
    shouldSaveConfig = false;
  }

  webSocket.beginSSL("websocket.ericbetts.dev", 443);
  webSocket.setReconnectInterval(5000);
  webSocket.onEvent(webSocketEvent);
}

void loop() {
  // put your main code here, to run repeatedly:
  webSocket.loop();
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    //keepalive
    webSocket.sendTXT("keepalive");
  }

  b.update();
  if (b.fell()) {
    triggerMp3();
  }
}
